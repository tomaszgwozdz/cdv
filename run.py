import subprocess

print("Klasyfikator spamu SMS: uruchamianie.")
subprocess.call("python sms_spam_init.py", shell=True)

print("Porównanie dokładności klasyfikatorów..")
subprocess.call("python compare_classification_algorithm.py", shell=True)

print("Tworzenie prognozy...")
subprocess.call("python sms_classification.py", shell=True)