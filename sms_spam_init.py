import re
import nltk
import string
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from wordcloud import WordCloud
from collections import Counter
from nltk.corpus import stopwords


def read_clean_dataset():
    try:
        # Odczyt pliku źródłowego i zapis do dataframe. Zbiór pobrany z https://www.kaggle.com/ishansoni/sms-spam-collection-dataset
        sms_df = pd.read_csv('sms-spam-collection-dataset/spam.csv', encoding='latin-1')
        # Usuwanie niepotrzebnych kolumn
        sms_df.drop(['Unnamed: 2', 'Unnamed: 3', 'Unnamed: 4'], axis=1, inplace=True)
        # Zmiana nazwy kolumn
        sms_df = sms_df.rename(columns={"v1": "label", "v2": "Message"})
        return sms_df
    except Exception as e:
        print(e)
        exit()


def plot_spam_ham(dataframe):
    try:
        sns.countplot(dataframe.label)
        plt.xlabel('Label')
        plt.title('Liczba wiadomości SPAM i NIESPAM')
        plt.show()
    except Exception as e:
        print(e)
        exit()


def word_count(text):
    try:
        text = text.lower()
        regex = re.compile('[' + re.escape(string.punctuation) + '0-9\\r\\t\\n]')
        txt = regex.sub(' ', text)  # usuwanie znaków interpunkcji
        # usuwanie stop wordsów i wyrazów o długości mniejszej niż 3 znaki
        words = [w for w in txt.split(' ') if not w in stop_words and len(w) > 3]
        return len(words)
    except Exception:
        return 0


def tokenize(text):
    exclude = set(string.punctuation)
    regex = re.compile('[' + re.escape(string.punctuation) + '0-9\\r\\t\\n]')  # Usuwanie interpunkcji
    text = regex.sub(' ', text)
    tokens = nltk.word_tokenize(text)  # Tokenizacja tekstu
    tokens = list(filter(lambda x: x.lower() not in stop_words, tokens))  # Usuwanie stop wordsów
    tokens = [w.lower() for w in tokens if len(w) >= 3]
    tokens = [w for w in tokens if re.search('[a-zA-Z]', w)]
    return tokens


if __name__ == '__main__':
    sms_df = read_clean_dataset()
    plot_spam_ham(sms_df)
    stop_words = set(stopwords.words('english'))
    spam_df = sms_df[sms_df['label'] == 'spam']  # sub-dataframe dla spamu
    ham_df = sms_df[sms_df['label'] == 'ham']  # sub-dataframe dla niespamu
    spam_df['len'] = spam_df['Message'].apply(lambda x: len([w for w in x.split(' ')]))
    ham_df['len'] = ham_df['Message'].apply(lambda x: len([w for w in x.split(' ')]))
    spam_df['processed_len'] = spam_df['Message'].apply(lambda x: word_count(x))
    ham_df['processed_len'] = ham_df['Message'].apply(lambda x: word_count(x))
    spam_df['tokens'] = spam_df['Message'].map(tokenize)
    ham_df['tokens'] = ham_df['Message'].map(tokenize)
    spam_words = []
    for token in spam_df['tokens']:
        spam_words = spam_words + token  # łączenie tekstu w różnych kolumnach na jednej liście
    ham_words = []
    for token in ham_df['tokens']:
        ham_words += token
    spam_count = Counter(spam_words).most_common(10)
    ham_count = Counter(ham_words).most_common(10)
    spam_count_df = pd.DataFrame(spam_count, columns=['word', 'count'])
    ham_count_df = pd.DataFrame(ham_count, columns=['word', 'count'])
    fig, (ax, ax1) = plt.subplots(1, 2, figsize=(18, 6))
    sns.barplot(x=spam_count_df['word'], y=spam_count_df['count'], ax=ax)
    ax.set_ylabel('count', fontsize=15)
    ax.set_xlabel('word', fontsize=15)
    ax.tick_params(labelsize=15)
    ax.set_title('spam top 10 words', fontsize=15)
    sns.barplot(x=ham_count_df['word'], y=ham_count_df['count'], ax=ax1)
    ax1.set_ylabel('count', fontsize=15)
    ax1.set_xlabel('word', fontsize=15)
    ax1.tick_params(labelsize=15)
    ax1.set_title('ham top 10 words', fontsize=15)
    plt.show()
    spam_words_str = ' '.join(spam_words)
    ham_words_str = ' '.join(ham_words)
    spam_word_cloud = WordCloud(width=600, height=400, background_color='white').generate(spam_words_str)
    ham_word_cloud = WordCloud(width=600, height=400, background_color='white').generate(ham_words_str)
    fig, (ax, ax2) = plt.subplots(1, 2, figsize=(18, 8))
    ax.imshow(spam_word_cloud)
    ax.axis('off')
    ax.set_title('spam word cloud', fontsize=20)
    ax2.imshow(ham_word_cloud)
    ax2.axis('off')
    ax2.set_title('ham word cloud', fontsize=20)
    plt.show()