Klasyfikator SPAMu - określanie, czy wiadomość sms jest typu SPAM czy HAM (nie spam).

Program przetwarza dane tekstowe w języku naturalnym, bazując na kolekcji z witryny Kaggle (https://www.kaggle.com/ishansoni/sms-spam-collection-dataset).

Pobrany tekst jest procesowany pod kątem usuwania znaków interpunkcji, wyrazów o nieistotnym znaczeniu oraz wyrazów, których długość nie przekracza 3 znaków. 
Tekst jest następnie tokenizowany za pomocą bilbioteki NLTK.

Program wykorzystuje 3 rodzaje klasyfikatorów:

1) Wielomianowy klasyfikator Naive Bayes - nadaje się do klasyfikacji z dyskretnymi funkcjami (np. Liczba słów do klasyfikacji tekstu). Rozkład wielomianowy zwykle wymaga zliczania funkcji całkowitych. Jednak w praktyce mogą również działać liczby ułamkowe, takie jak tf-idf.

2) Losowy las - estymator, który dopasowuje do wielu klasyfikatorów drzew decyzyjnych na różnych podpróbach zestawu danych i wykorzystuje uśrednianie, aby poprawić dokładność predykcyjną i kontrolę dopasowania. Wielkość podpróbki jest zawsze taka sama jak pierwotna wielkość próbki wejściowej.

3) Klasyfikator k-najbliższych sąsiadów.