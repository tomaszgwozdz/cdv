import pandas as pd
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer

def train_classifier(dataframe):
    try:
        # Naiwny klasyfikator Bayesa
        classifier = MultinomialNB()
        x_train_counts = count_vect.fit_transform(dataframe['v2'])
        classifier.fit(x_train_counts, dataframe['v1'])
        return classifier
    except Exception as e:
        print(e)
        exit()


def make_prediction(text, classifier):
    try:
        return classifier.predict(count_vect.transform([text]))[0]
    except Exception as e:
        print(e)
        exit()


if __name__ == '__main__':
    count_vect = CountVectorizer()
    data = pd.read_csv('sms-spam-collection-dataset/spam.csv', encoding='latin-1')
    classifier = train_classifier(dataframe=data)
    print("Klasyfikacja wiadomości: Hey, I want send money to you")
    print(make_prediction("Hey, I want send money to you", classifier=classifier))