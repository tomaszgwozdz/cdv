import pandas as pd
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer


def perform(classifiers, vectorizers, train_data, test_data):
    # Funkcja trenuje model za pomocą dostarczonego wektora i wypisuje wynik
    for classifier in classifiers:
        for vectorizer in vectorizers:
            string = ''
            string += classifier.__class__.__name__ + ' z ' + vectorizer.__class__.__name__

            # trening
            vectorize_text = vectorizer.fit_transform(train_data.v2)
            classifier.fit(vectorize_text, train_data.v1)

            # wynik
            vectorize_text = vectorizer.transform(test_data.v2)
            score = classifier.score(vectorize_text, test_data.v1)
            string += '. Wynik: ' + str(score)
            print(string)


if __name__ == '__main__':
    data = pd.read_csv('sms-spam-collection-dataset/spam.csv', encoding='latin-1')
    learn = data[:4400]  # 4400 wiadomości(Zestaw treningowy)
    test = data[4400:]  # 1172 wiadomości (Zestaw testowy)

    perform(
        [
            RandomForestClassifier(n_estimators=100, n_jobs=-1),
            KNeighborsClassifier(),
            MultinomialNB()
        ],
        [
            CountVectorizer(),
            TfidfVectorizer()
        ],
        learn,
        test
    )